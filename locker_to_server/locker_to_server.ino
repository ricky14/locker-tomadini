#include <SPI.h>
#include <RFID.h>

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include <Servo.h> //Inserire la libreria Servo

Servo Servo1; //Il nome del servo è Servo1
static const uint8_t OPEN = 0;
static const uint8_t CLOSE = 150;


static const uint8_t D0 = 16;
static const uint8_t D1 = 5;
static const uint8_t D2 = 4;
static const uint8_t D3 = 0;
static const uint8_t D4 = 2;
static const uint8_t D5 = 14;
static const uint8_t D6 = 12;
static const uint8_t D7 = 13;
static const uint8_t D8 = 15;
static const uint8_t SD2 = 9;

const char* ssid = "Wi-Fi network ssid";
const char* password = "Wi-Fi password";

//Your Domain name with URL path or IP address with path
String serverName = "http://192.168.120.12:8080/";

unsigned long lastTime = 0;
// Timer set to 10 minutes (600000)
//unsigned long timerDelay = 600000;
// Set timer to 5 seconds (5000)
unsigned long timerDelay = 5000;

#define SS_PIN D8
#define RST_PIN D0

byte nuidPICC[4];

RFID rfid(SS_PIN, RST_PIN);  // Instance of the class

// Init array that will store new NUID

#define button_oc D2
#define led_oc D3
#define led_pa D1

String tag_id = "0";
String locker_id = WiFi.macAddress();
bool is_tag_inside = false;


void setup() {

  Servo1.attach (D4); 
  Servo1.write (CLOSE);
  delay(1000);
  Serial.println("0");

  pinMode(led_oc, OUTPUT);
  pinMode(led_pa, OUTPUT);
  pinMode(button_oc, INPUT);

  digitalWrite(led_oc, LOW);
  digitalWrite(led_pa, LOW);

  Serial.begin(9600);
  SPI.begin();
  rfid.init();
  Serial.println();
  Serial.println("RFID reader setup finished");

  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.println("Connecting");

  int i=0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if(i==0){
      digitalWrite(led_oc, HIGH);
      i=1;
    }else{
      digitalWrite(led_oc, LOW);
      i=0;
    }
  }
  digitalWrite(led_oc, LOW);
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

  int connected = 0;
  while(connected == 0){
    if (WiFi.status() == WL_CONNECTED) {

      WiFiClient client;
      HTTPClient http;

      String serverPath = serverName + "connect_locker?locker_id=" +
        locker_id + "&tag_id=" + tag_id;
      
      // Your Domain name with URL path or IP address with path
      http.begin(client, serverPath.c_str());

        
      // Send HTTP GET request
      int httpResponseCode = http.GET();
      
      if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);
        tag_id = payload;
        connected = 1 ;
      }
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
        int i=0;
        for(int c=0; c<4; c++){
          delay(500);
          Serial.println(".");
          if(i==0){
            digitalWrite(led_oc, HIGH);
            i=1;
          }else{
            digitalWrite(led_oc, LOW);
            i=0;
          }
        }
      }
      // Free resources
      http.end();
      

    } else {
      Serial.println("WiFi Disconnected");
      WiFi.begin(ssid, password);
      Serial.println("Connecting");
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        if(i==0){
          digitalWrite(led_oc, HIGH);
          i=1;
        }else{
          digitalWrite(led_oc, LOW);
          i=0;
        }
      }
      Serial.println("");
      Serial.print("Connected to WiFi network with IP Address: ");
      Serial.println(WiFi.localIP());
    }
  }
    

  int check = 0;
  while(check == 0){
    if (WiFi.status() == WL_CONNECTED) {

      if (!rfid.isCard()){
        Serial.println("No tag inside the locker");
        is_tag_inside = false;
      }else{
        Serial.println("The tag is inside the locker");
        is_tag_inside = true;

        // Verify if the NUID has been readed
        while (!rfid.readCardSerial()){yield();}
        
        for (byte i = 0; i < 4; i++) {
          nuidPICC[i] = rfid.serNum[i];
        }
        Serial.println(F("A tag has been detected."));
        // Store NUID into nuidPICC array

        Serial.println(F("The NUID tag is:"));
        Serial.print(F("In hex: "));
        printHex(rfid.serNum, 4);
        Serial.println();
        Serial.print(F("In dec: "));
        printDec(rfid.serNum, 4);
        Serial.println();
        
        
      }
        
      rfid.halt();
      


      WiFiClient client;
      HTTPClient http;

      String serverPath = serverName + "tag_setup?locker_id=" +
        locker_id + "&tag_id=" + tag_id ;
      
      if(is_tag_inside){
        serverPath = serverPath + "&tag_in=true" ;
      }else{
        serverPath = serverPath + "&tag_in=false" ;
      }

      // Your Domain name with URL path or IP address with path
      http.begin(client, serverPath.c_str());

        
      // Send HTTP GET request
      int httpResponseCode = http.GET();
      
      if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);
        check = 1;

      }
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
        int i=0;
        for(int c=0; c<4; c++){
          delay(500);
          Serial.println(".");
          if(i==0){
            digitalWrite(led_oc, HIGH);
            i=1;
          }else{
            digitalWrite(led_oc, LOW);
            i=0;
          }
        }
      }
      // Free resources
      http.end();
      

    } else {
      Serial.println("WiFi Disconnected");
      WiFi.begin(ssid, password);
      Serial.println("Connecting");
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        if(i==0){
          digitalWrite(led_oc, HIGH);
          i=1;
        }else{
          digitalWrite(led_oc, LOW);
          i=0;
        }
      }
      Serial.println("");
      Serial.print("Connected to WiFi network with IP Address: ");
      Serial.println(WiFi.localIP());
    }
  }

  if(is_tag_inside){
    digitalWrite(led_pa, HIGH);
  }else{
    digitalWrite(led_pa, LOW);
  }
  
  Servo1.write (CLOSE);

}


WiFiClient client;
HTTPClient http;
String serverPath;
int httpResponseCode;
String payload;
bool open;
String read_tag;
int c;
int i;


int pos = 0;
int dest = 0;
void move_servo_to(){
  if(pos<dest){
    while(pos<dest){
      pos = pos+1;
      Servo1.write(pos);
      delay(10);
    }
  }else{
    while(pos>dest){
      pos = pos-1;
      Servo1.write(pos);
      delay(10);
    }
  }
  Servo1.write(dest);
}

void loop() {

  if (WiFi.status() == WL_CONNECTED) {
    if(digitalRead(button_oc) == LOW)
      return;

    while(digitalRead(button_oc) == HIGH){yield();}

    client;
    http;

    serverPath = serverName + "open?locker_id=" + locker_id;
    
    // Your Domain name with URL path or IP address with path
    http.begin(client, serverPath.c_str());
      
    // Send HTTP GET request
    int httpResponseCode = http.GET();
    
    if (httpResponseCode>0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      payload = http.getString();
      Serial.println(payload);
      if(payload[0] == '1'){
        Serial.println("Access to locker agreed");
        digitalWrite(led_oc, HIGH);
        dest = OPEN;
        Serial.println("OPEN");
        move_servo_to();

        tag_id = payload.substring(2);

        open = true;
        while(open){
          yield();
          while(digitalRead(button_oc) == LOW){yield();}
          while(digitalRead(button_oc) == HIGH){yield();}

          if (!rfid.isCard()){
            Serial.println("Nothing is inside the locker");
            open = false;
            digitalWrite(led_pa, LOW);
            is_tag_inside = false;
            
          }else{

            while(!rfid.readCardSerial()){yield();};
          
            for (byte i = 0; i < 4; i++) {
              nuidPICC[i] = rfid.serNum[i];
            }
            Serial.println(F("A new tag has been detected."));

            Serial.println(F("The NUID tag is:"));
            Serial.print(F("In hex: "));
            printHex(rfid.serNum, 4);
            Serial.println();
            Serial.print(F("In dec: "));
            printDec(rfid.serNum, 4);
            Serial.println();
            
            read_tag = "";
            read_tag = read_tag + rfid.serNum[0] + "-" + rfid.serNum[1] + "-" + rfid.serNum[2] + "-" + rfid.serNum[3];

            if(tag_id.equals(read_tag)){
              Serial.println("Rigth tag inside");
              digitalWrite(led_pa, HIGH);
              is_tag_inside = true;
              open = false;
            }else{
              Serial.println("Wrong tag inside");
              digitalWrite(led_pa, LOW);
              is_tag_inside = false;
              open = true;
              for(c = 0; c < 10; c++){
                digitalWrite(led_pa, HIGH);
                delay(100);
                digitalWrite(led_pa, LOW);
                delay(100);
              }
            }
          }
            
          rfid.halt();

          if(!open){
            http.end();

            serverPath = serverName + "close?locker_id=" + locker_id + "&tag_id=" +
              nuidPICC[0] + "-" +
              nuidPICC[1] + "-" +
              nuidPICC[2] + "-" +
              nuidPICC[3] + "&tag_in=" + is_tag_inside;
  
            // Your Domain name with URL path or IP address with path
            http.begin(client, serverPath.c_str());

            // If you need Node-RED/server authentication, insert user and password below
            //http.setAuthorization("REPLACE_WITH_SERVER_USERNAME", "REPLACE_WITH_SERVER_PASSWORD");
              
            // Send HTTP GET request
            int httpResponseCode = http.GET();
            
            if (httpResponseCode>0) {
              Serial.print("HTTP Response code: ");
              Serial.println(httpResponseCode);
              String payload = http.getString();
              Serial.println(payload);
              if(payload[0] == '1'){
                Serial.println("Locker locking agreed");
                dest = CLOSE;
                Serial.println("CLOSE");
                move_servo_to();
                digitalWrite(led_oc, LOW);
              }else{
                if(payload[0] == '0'){
                  Serial.println("No open locker");
                  digitalWrite(led_oc, HIGH);
                  open = true;
                }else{
                  Serial.println("Wrong tag inside");
                  digitalWrite(led_pa, LOW);
                  open = true;
                  for(c = 0; c < 10; c++){
                    digitalWrite(led_pa, HIGH);
                    delay(100);
                    digitalWrite(led_pa, LOW);
                    delay(100);
                  }
                }
              }
            }else{
              Serial.print("Error code: ");
              Serial.println(httpResponseCode);
              for(c = 0; c < 3; c++){
                digitalWrite(led_oc, LOW);
                delay(100);
                digitalWrite(led_oc, HIGH);
                delay(100);
              }
              open = true;
            }
          }
        
        }
      


      }else{
        Serial.println("Access to locker denied");
        for(c = 0; c < 10; c++){
          digitalWrite(led_oc, HIGH);
          delay(100);
          digitalWrite(led_oc, LOW);
          delay(100);
        }
        
      }
    }else{
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
      for(c = 0; c < 3; c++){
        digitalWrite(led_oc, HIGH);
        delay(100);
        digitalWrite(led_oc, LOW);
        delay(100);
      }
    }
    // Free resources
    http.end();
    

  }else{
    Serial.println("WiFi Disconnected");
    WiFi.begin(ssid, password);
    Serial.println("Connecting");
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
      if(i==0){
        digitalWrite(led_oc, HIGH);
        i=1;
      }else{
        digitalWrite(led_oc, LOW);
        i=0;
      }
    }
    Serial.println("");
    Serial.print("Connected to WiFi network with IP Address: ");
    Serial.println(WiFi.localIP());
  }

}




/**
 		Helper routine to dump a byte array as hex values to Serial.
*/
void printHex(byte* buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}
/**
 		Helper routine to dump a byte array as dec values to Serial.
*/
void printDec(byte* buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], DEC);
  }
}