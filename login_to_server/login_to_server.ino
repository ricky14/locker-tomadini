#include <SPI.h>
#include <MFRC522.h>

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

static const uint8_t D0 = 16;
static const uint8_t D1 = 5;
static const uint8_t D2 = 4;
static const uint8_t D3 = 0;
static const uint8_t D4 = 2;
static const uint8_t D5 = 14;
static const uint8_t D6 = 12;
static const uint8_t D7 = 13;
static const uint8_t D8 = 15;

const char* ssid = "Wi-Fi network ssid";
const char* password = "Wi-Fi password";

//Your Domain name with URL path or IP address with path
String serverName = "http://192.168.120.12:8080/";

unsigned long lastTime = 0;
// Timer set to 10 minutes (600000)
//unsigned long timerDelay = 600000;
// Set timer to 5 seconds (5000)
unsigned long timerDelay = 5000;

#define SS_PIN D4
#define RST_PIN D0
MFRC522 rfid(SS_PIN, RST_PIN);  // Instance of the class
MFRC522::MIFARE_Key key;
// Init array that will store new NUID

#define led_r D1 
#define led_y D2
#define led_g D3


void setup() {


  pinMode(led_r, OUTPUT);
  pinMode(led_y, OUTPUT);
  pinMode(led_g, OUTPUT);

  digitalWrite(led_r, LOW);
  digitalWrite(led_y, LOW);
  digitalWrite(led_g, LOW);

  Serial.begin(9600);
  SPI.begin();      // Init SPI bus
  rfid.PCD_Init();  // Init MFRC522
  Serial.println();
  Serial.print(F("Reader :"));
  rfid.PCD_DumpVersionToSerial();
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
  Serial.println();
  Serial.println(F("This code scan the MIFARE Classic NUID."));
  Serial.print(F("Using the following key:"));
  printHex(key.keyByte, MFRC522::MF_KEY_SIZE);

  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.println("Connecting");
  int i=0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    if(i==0){
      digitalWrite(led_y, HIGH);
      i=1;
    }else{
      digitalWrite(led_y, LOW);
      i=0;
    }
    Serial.print(".");
  }
  Serial.println("");
  digitalWrite(led_y, HIGH);
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

}


int i = 0;
WiFiClient client;
HTTPClient http;
String serverPath;
String payload;

void loop() {

  if (WiFi.status() == WL_CONNECTED) {

    // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
    if (!rfid.PICC_IsNewCardPresent())
      return;
    
    digitalWrite(led_y, LOW);
    
    // Verify if the NUID has been readed
    if (!rfid.PICC_ReadCardSerial())
      return;
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
    Serial.println(rfid.PICC_GetTypeName(piccType));
    // Check is the PICC of Classic MIFARE type
    if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI && piccType != MFRC522::PICC_TYPE_MIFARE_1K && piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
      Serial.println(F("Your tag is not of type MIFARE Classic."));
      digitalWrite(led_y, HIGH);

      return;
    }
    Serial.println(F("A new card has been detected."));
    // Store NUID into nuidPICC array

    Serial.println(F("The NUID tag is:"));
    Serial.print(F("In hex: "));
    printHex(rfid.uid.uidByte, rfid.uid.size);
    Serial.println();
    Serial.print(F("In dec: "));
    printDec(rfid.uid.uidByte, rfid.uid.size);
    Serial.println();


    serverPath = serverName + "login?id=" +
      rfid.uid.uidByte[0] + "-" +
      rfid.uid.uidByte[1] + "-" +
      rfid.uid.uidByte[2] + "-" +
      rfid.uid.uidByte[3];
    
    // Your Domain name with URL path or IP address with path
    http.begin(client, serverPath.c_str());

    // If you need Node-RED/server authentication, insert user and password below
    //http.setAuthorization("REPLACE_WITH_SERVER_USERNAME", "REPLACE_WITH_SERVER_PASSWORD");
      
    // Send HTTP GET request
    int httpResponseCode = http.GET();
    
    if (httpResponseCode>0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      payload = http.getString();
      Serial.println(payload);
      if(payload[0]=='1'){
        digitalWrite(led_g, HIGH);
        delay(3000);
        serverPath = serverName + "isLogged";
        bool logged = true;
        while(logged){
          http.end();
          http.begin(client, serverPath.c_str());
          int httpResponseCode = http.GET();
          if (httpResponseCode>0) {
            Serial.print("HTTP Response code: ");
            Serial.println(httpResponseCode);
            payload = http.getString();
            Serial.println(payload);
            if(payload[0]=='1'){
              digitalWrite(led_g, HIGH);
            }else{
              digitalWrite(led_g, LOW);
              logged = false;
            }
          } else {
            Serial.print("Error code: ");
            Serial.println(httpResponseCode);
            for(i=0; i<4; i++){
              digitalWrite(led_y, LOW);
              delay(250);
              digitalWrite(led_y, HIGH);
              delay(250);
            }

          }
          delay(1500);
        }
        digitalWrite(led_g, LOW);
      }else{
        digitalWrite(led_r, HIGH);
        delay(3000);
        digitalWrite(led_r, LOW);
      }
    }
    else {
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
      for(i=0; i<4; i++){
        digitalWrite(led_y, LOW);
        delay(250);
        digitalWrite(led_y, HIGH);
        delay(250);
      }

    }
    digitalWrite(led_y, HIGH);
    // Free resources
    http.end();
    



    // Halt PICC
    rfid.PICC_HaltA();
    // Stop encryption on PCD
    rfid.PCD_StopCrypto1();

  } else {
    Serial.println("WiFi Disconnected");
    WiFi.begin(ssid, password);
    Serial.println("Connecting");
    int i=0;
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      if(i==0){
        digitalWrite(led_y, HIGH);
        i=1;
      }else{
        digitalWrite(led_y, LOW);
        i=0;
      }
      Serial.print(".");
    }
    digitalWrite(led_y, HIGH);
    Serial.println("");
    Serial.print("Connected to WiFi network with IP Address: ");
    Serial.println(WiFi.localIP());
  }

}




/**
 		Helper routine to dump a byte array as hex values to Serial.
*/
void printHex(byte* buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}
/**
 		Helper routine to dump a byte array as dec values to Serial.
*/
void printDec(byte* buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], DEC);
  }
}