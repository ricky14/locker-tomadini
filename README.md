# Locker-Tomadini

This is an Internet Of Things project that uses an ESP8266 microcontroller with RFID RC522 modules to open and close a locker. A Node.js Server, that uses Express, pg and ejs, is the core of the system.

Steps to get started:
- Configure the login and locker modules
- Install into the ESP8266 the login_to_server and locker_to_server Arduino programs (insert inside the code the right URL/IP address of the server)
- Run the index.js file to start the server
- Enjoy it
