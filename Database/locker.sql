--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: access; Type: TABLE; Schema: public; Owner: rickyross
--

CREATE TABLE public.access (
    card character varying DEFAULT 0 NOT NULL,
    locker character varying,
    key character varying,
    keyinbefore boolean,
    keyinafter boolean,
    timeopen character varying NOT NULL,
    timeclose character varying
);


ALTER TABLE public.access OWNER TO rickyross;

--
-- Name: card; Type: TABLE; Schema: public; Owner: rickyross
--

CREATE TABLE public.card (
    id character varying NOT NULL,
    room character varying
);


ALTER TABLE public.card OWNER TO rickyross;

--
-- Name: key; Type: TABLE; Schema: public; Owner: rickyross
--

CREATE TABLE public.key (
    id character varying NOT NULL,
    nickname character varying NOT NULL
);


ALTER TABLE public.key OWNER TO rickyross;

--
-- Name: locker; Type: TABLE; Schema: public; Owner: rickyross
--

CREATE TABLE public.locker (
    id character varying NOT NULL,
    key character varying DEFAULT 0,
    open boolean NOT NULL,
    keyin boolean NOT NULL,
    enabled boolean NOT NULL
);


ALTER TABLE public.locker OWNER TO rickyross;

--
-- Name: logged; Type: TABLE; Schema: public; Owner: rickyross
--

CREATE TABLE public.logged (
    card character varying NOT NULL,
    "time" character varying
);


ALTER TABLE public.logged OWNER TO rickyross;

--
-- Name: room; Type: TABLE; Schema: public; Owner: rickyross
--

CREATE TABLE public.room (
    id character varying NOT NULL,
    resident character varying
);


ALTER TABLE public.room OWNER TO rickyross;

--
-- Data for Name: access; Type: TABLE DATA; Schema: public; Owner: rickyross
--

COPY public.access (card, locker, key, keyinbefore, keyinafter, timeopen, timeclose) FROM stdin;
\.


--
-- Data for Name: card; Type: TABLE DATA; Schema: public; Owner: rickyross
--

COPY public.card (id, room) FROM stdin;
0	0
\.


--
-- Data for Name: key; Type: TABLE DATA; Schema: public; Owner: rickyross
--

COPY public.key (id, nickname) FROM stdin;
0	factory-key-locker
\.


--
-- Data for Name: locker; Type: TABLE DATA; Schema: public; Owner: rickyross
--

COPY public.locker (id, key, open, keyin, enabled) FROM stdin;
\.


--
-- Data for Name: logged; Type: TABLE DATA; Schema: public; Owner: rickyross
--

COPY public.logged (card, "time") FROM stdin;
\.


--
-- Data for Name: room; Type: TABLE DATA; Schema: public; Owner: rickyross
--

COPY public.room (id, resident) FROM stdin;
0	System
\.


--
-- Name: access Access_pk; Type: CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.access
    ADD CONSTRAINT "Access_pk" PRIMARY KEY (card, timeopen);


--
-- Name: card Card_pkey; Type: CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.card
    ADD CONSTRAINT "Card_pkey" PRIMARY KEY (id);


--
-- Name: key Key_pkey; Type: CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.key
    ADD CONSTRAINT "Key_pkey" PRIMARY KEY (id);


--
-- Name: locker Locker_pkey; Type: CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.locker
    ADD CONSTRAINT "Locker_pkey" PRIMARY KEY (id);


--
-- Name: logged Logged_pkey; Type: CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.logged
    ADD CONSTRAINT "Logged_pkey" PRIMARY KEY (card);


--
-- Name: room Room_pkey; Type: CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.room
    ADD CONSTRAINT "Room_pkey" PRIMARY KEY (id);


--
-- Name: fki_C; Type: INDEX; Schema: public; Owner: rickyross
--

CREATE INDEX "fki_C" ON public.access USING btree (card);


--
-- Name: fki_Key_fk; Type: INDEX; Schema: public; Owner: rickyross
--

CREATE INDEX "fki_Key_fk" ON public.access USING btree (key);


--
-- Name: fki_Locker_fk; Type: INDEX; Schema: public; Owner: rickyross
--

CREATE INDEX "fki_Locker_fk" ON public.access USING btree (locker);


--
-- Name: logged Card_fk; Type: FK CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.logged
    ADD CONSTRAINT "Card_fk" FOREIGN KEY (card) REFERENCES public.card(id);


--
-- Name: access Card_fk; Type: FK CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.access
    ADD CONSTRAINT "Card_fk" FOREIGN KEY (card) REFERENCES public.card(id) ON DELETE SET DEFAULT NOT VALID;


--
-- Name: access Key_fk; Type: FK CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.access
    ADD CONSTRAINT "Key_fk" FOREIGN KEY (key) REFERENCES public.key(id) ON DELETE CASCADE NOT VALID;


--
-- Name: locker Key_fk; Type: FK CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.locker
    ADD CONSTRAINT "Key_fk" FOREIGN KEY (key) REFERENCES public.key(id) ON DELETE SET DEFAULT NOT VALID;


--
-- Name: access Locker_fk; Type: FK CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.access
    ADD CONSTRAINT "Locker_fk" FOREIGN KEY (locker) REFERENCES public.locker(id) ON DELETE SET NULL NOT VALID;


--
-- Name: card Room_fk; Type: FK CONSTRAINT; Schema: public; Owner: rickyross
--

ALTER TABLE ONLY public.card
    ADD CONSTRAINT "Room_fk" FOREIGN KEY (room) REFERENCES public.room(id) NOT VALID;


--
-- PostgreSQL database dump complete
--

