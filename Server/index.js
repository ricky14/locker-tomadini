const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 8080;

app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

const Pool = require('pg').Pool
const pool = new Pool({
    user: 'rickyross',
    host: 'localhost',
    database: 'Locker',
    port: 5432,
})

let last_card_read = "";

app.get('', (req, res) => {
    res.render(__dirname + "/views/home.ejs", {});
});

app.get('/style.css', (req, res) => {
    res.sendFile(__dirname + "/views/style.css");
});

app.get('/card_read', (req, res) => {
    console.log(req.query.id);
    res.send("OK: " + req.query.id);
});




const getAccess = (req, res) => {
    let queryAllAccess = "SELECT a.locker, a.key, k.nickname, a.card AS card_id, a.card, c.room, r.resident, a.keyinbefore, a.keyinafter, a.timeopen, a.timeclose " +
        "FROM access AS a JOIN key AS k ON a.key = k.id JOIN card AS c ON a.card = c.id JOIN room AS r ON c.room = r.id " +
        "ORDER BY a.timeopen DESC"
    if (req.query.filter == "key") {
        //console.log("Filter key: "+req.query.key_id);
        queryAllAccess = "SELECT a.locker, a.key, k.nickname, a.card AS card_id, a.card, c.room, r.resident, a.keyinbefore, a.keyinafter, a.timeopen, a.timeclose " +
            "FROM access AS a JOIN key AS k ON a.key = k.id JOIN card AS c ON a.card = c.id JOIN room AS r ON c.room = r.id " + "WHERE a.key = '" + req.query.key_id + "' " +
            "ORDER BY a.timeopen DESC";
    } else if (req.query.filter == "room") {
        //console.log("Filter room: "+req.query.card_id);
        queryAllAccess = "SELECT a.locker, a.key, k.nickname, a.card AS card_id, a.card, c.room, r.resident, a.keyinbefore, a.keyinafter, a.timeopen, a.timeclose " +
            "FROM access AS a JOIN key AS k ON a.key = k.id JOIN card AS c ON a.card = c.id JOIN room AS r ON c.room = r.id " + "WHERE a.card = '" + req.query.card_id + "' " +
            "ORDER BY a.timeopen DESC"
    }
    pool.query(queryAllAccess, (error, results) => {
        //console.log(queryAllAccess);
        if (error) {
            console.log(error);
            res.send(error);
        } else {
            let access = results.rows;
            pool.query("SELECT c.id AS card_id, r.id, r.resident FROM card AS c JOIN room AS r on c.room = r.id", (error, results) => {
                if (error) {
                    res.send(error);
                } else {
                    let rooms = results.rows;
                    pool.query("SELECT * FROM key ", (error, results) => {
                        if (error) {
                            res.send(error);
                        } else {
                            let keys = results.rows;
                            res.status(200).render(
                                __dirname + "/views/access_list.ejs",
                                { 'last_card': last_card_read, 'rows': access, 'rooms': rooms, 'keys': keys });
                        }
                    })
                }
            })
        }

    })
}

app.get('/getAccess', getAccess);

app.post('/removeAccess', (req, res) => {
    let card_id = req.body.card_id;
    let timeopen = req.body.timeopen;
    if (card_id == 'undefined' | timeopen == 'undefined') {
        res.send("Missing data");
    } else {
        pool.query('DELETE FROM access WHERE card = $1 AND timeopen = $2', [card_id, timeopen],
            (error, results) => {
                if (error) {
                    res.send(error);
                } else {
                    res.redirect('/getAccess');
                }
            })
    }
})




app.get('/create_key_page', (req, res) => {
    let key_id = req.query.key_id;
    if (key_id == "undefined" | key_id == "") {
        res.redirect('/getAccess');
    } else {
        pool.query('SELECT * FROM locker', (error, results) => {
            if (error) {
                res.send(error);
            } else
                res.status(200).render(
                    __dirname + "/views/new_key.ejs",
                    { 'key_id': key_id, 'lockers': results.rows });
        })
    }
})

app.post('/new_key', (req, res) => {
    let key_id = req.body.key_id;
    let key_nickname = req.body.key_nickname;
    let locker_id = req.body.locker;
    pool.query('INSERT INTO key (id, nickname) values ($1, $2)', [key_id, key_nickname], (error, results) => {
        if (error) {
            console.log(error);
            res.send(error);
        } else if (req.body.bind == "true") {
            console.log("New Key: key created, binding...");
            pool.query('UPDATE locker SET key = $1 WHERE id = $2', [key_id, locker_id], (error, results) => {
                if (error) {
                    console.log(error);
                    res.send(error);
                } else {
                    res.redirect('/getKey');
                }

            })
        } else {
            console.log("New Key: key created, no binding");
            res.redirect('/getKey');
        }

    })
})

app.get('/getKey', (req, res) => {
    pool.query('SELECT k.id, k.nickname, l.id AS locker FROM key AS k LEFT OUTER JOIN locker AS l ON k.id = l.key ORDER BY k.id',
        (error, results) => {
            if (error) {
                res.send(error);
            } else {
                res.status(200).render(
                    __dirname + "/views/keys.ejs",
                    { 'keys': results.rows });
            }

        })
})

app.post('/removeKey', (req, res) => {
    if (req.body.key_id == '0' | req.body.key_id == 'undefined') {
        res.send('Key not found or protected');
    } else
        pool.query('DELETE FROM key WHERE id = $1', [req.body.key_id],
            (error, results) => {
                if (error) {
                    res.send(error);
                } else {
                    res.redirect('/getKey');
                }
            })
})




app.get('/getLocker', (req, res) => {
    pool.query('SELECT l.id, k.nickname, open, keyin, enabled FROM locker AS l LEFT OUTER JOIN key AS k ON l.key = k.id ORDER BY l.id',
        (error, results) => {
            if (error) {
                res.send(error);
            }
            res.status(200).render(
                __dirname + "/views/lockers.ejs",
                { 'lockers': results.rows });
        })
})

app.post('/removeLocker', (req, res) => {
    pool.query('DELETE FROM locker WHERE id = $1', [req.body.locker_id],
        (error, results) => {
            if (error) {
                res.send(error);
            } else {
                res.redirect('/getLocker');
            }
        })
})

app.post('/bindLockerKey', (req, res) => {
    let locker_id = req.body.locker_id;
    let key_id = req.body.key_id;

    pool.query('SELECT * FROM locker WHERE keyin AND enabled AND id = $1', [locker_id], (error, results) => {
        if (error) {
            throw error
        }
        if (results.rows.length > 0) {
            res.send("The locker has the key in, remove and try again");
        } else {
            pool.query('UPDATE locker SET key = $1 WHERE id = $2', [key_id, locker_id],
                (error, results) => {
                    if (error) {
                        res.send(error);
                    } else {
                        res.redirect('/getLocker');
                    }
                })
        }
    })


})






app.get('/create_card_page', (req, res) => {
    let card_id = req.query.card_id;
    if (card_id == "undefined" | card_id == "") {
        res.redirect('/getAccess');
    } else {
        pool.query('SELECT * FROM room', (error, results) => {
            if (error) {
                res.send(error);
            }
            res.status(200).render(
                __dirname + "/views/new_card.ejs",
                { 'card_id': card_id, 'rooms': results.rows });
        })
    }

})

app.post('/new_card', (req, res) => {
    let card_id = req.body.card_id;
    let room = req.body.room.split("$$$");
    pool.query('INSERT INTO card (id, room) values ($1, $2)', [card_id, room[0]], (error, results) => {
        if (error) {
            res.send(error);
        } else
            res.status(200).redirect('/getCard');
    })
})

app.post('/new_card_and_room', (req, res) => {
    let card_id = req.body.card_id;
    let room = [req.body.room_id, req.body.room_resident];
    pool.query('INSERT INTO room (id, resident) values ($1, $2)', room, (error, results) => {
        if (error) {
            res.send(error);
        } else
            pool.query('INSERT INTO card (id, room) values ($1, $2)', [card_id, room[0]], (error, results) => {
                if (error) {
                    res.send(error);
                }
                res.status(200).redirect('/getCard');
            })
    })
})

app.get('/getCard', (req, res) => {
    pool.query('SELECT c.id, c.room, r.resident FROM card AS c JOIN room AS r ON c.room = r.id ORDER BY r.id',
        (error, results) => {
            if (error) {
                res.send(error);
            } else
                res.status(200).render(
                    __dirname + "/views/cards.ejs",
                    { 'cards': results.rows });
        })
})

app.post('/removeCard', (req, res) => {
    let card_id = req.body.card_id;
    if (card_id == 0 | card_id == "undefined") {
        res.send('Card not found or protected');
    } else {
        pool.query('DELETE FROM card WHERE id = $1', [card_id],
            (error, results) => {
                if (error) {
                    res.send(error);
                } else {
                    res.redirect('/getCard');
                }
            })
    }
})



app.get('/getRoom', (req, res) => {
    pool.query('SELECT r.id, r.resident, c.id AS card FROM room AS r LEFT OUTER JOIN card AS c ON r.id = c.room ORDER BY r.id',
        (error, results) => {
            if (error) {
                res.send(error);
            }
            res.status(200).render(
                __dirname + "/views/rooms.ejs",
                { 'rooms': results.rows });
        })
})

app.post('/newRoom', (req, res) => {
    pool.query('INSERT INTO room (id, resident) values ($1, $2)', [req.body.room_id, req.body.room_resident],
        (error, results) => {
            if (error) {
                res.send(error);
            } else {
                res.redirect('/getRoom');
            }
        })
})

app.post('/removeRoom', (req, res) => {
    let room_id = req.body.room_id;
    if (room_id == 0 | room_id == "undefined") {
        res.send('Card not found or protected');
    } else {
        pool.query('DELETE FROM room WHERE id = $1', [room_id],
            (error, results) => {
                if (error) {
                    res.send(error);
                } else {
                    res.redirect('/getRoom');
                }
            })
    }
})

app.post('/bindCardRoom', (req, res) => {
    let card_id = req.body.card_id;
    let room_id = req.body.room_id;
    if (card_id == '0' | room_id == '0') {
        res.send("Editing system entries is not allowed");
    } else {
        pool.query('UPDATE card SET room = $1 WHERE id = $2', [room_id, card_id],
            (error, results) => {
                if (error) {
                    res.send(error);
                } else {
                    res.redirect('/getRoom');
                }
            })
    }

})





app.get('/editPage', (req, res) => {
    pool.query('SELECT * FROM card ORDER BY card.room',
        (error, results) => {
            if (error) {
                res.send(error);
            } else {
                let cards = results.rows;
                pool.query('SELECT * FROM room ORDER BY room.id',
                    (error, results) => {
                        if (error) {
                            res.send(error);
                        } else {
                            let rooms = results.rows;
                            pool.query('SELECT locker.id, key.nickname AS key FROM locker JOIN key ON locker.key = key.id ORDER BY locker.id',
                                (error, results) => {
                                    if (error) {
                                        res.send(error);
                                    } else {
                                        let lockers = results.rows;
                                        pool.query('SELECT * FROM key ORDER BY key.id',
                                            (error, results) => {
                                                if (error) {
                                                    res.send(error);
                                                } else {
                                                    let keys = results.rows;
                                                    res.status(200).render(
                                                        __dirname + "/views/editPage.ejs",
                                                        { 'rooms': rooms, 'cards': cards, 'lockers': lockers, 'keys': keys });
                                                }
                                            })
                                    }
                                })
                        }
                    })
            }
        })
})






//########################################  Arduino endpoints  ########################################

app.get('/login', (req, res) => {
    console.log(req.query.id);
    if (req.query.id == 'undefined') {
        console.log("Login: id undefned");
        res.send("0");
    } else {
        last_card_read = req.query.id;
        console.log("Login: " + req.query.id);
        pool.query('SELECT * FROM card WHERE id = $1', [req.query.id], (error, results) => {
            if (error) {
                throw error
            }
            if (results.rows.length == 0) {
                console.log("Login: no matching id");
                res.send("0");
            } else {
                pool.query('SELECT * FROM logged', (error, results) => {
                    if (error) {
                        throw error
                    }
                    let logged = results.rows;
                    pool.query('SELECT * FROM locker WHERE open', (error, results) => {
                        if (error) {
                            throw error
                        }
                        if (results.rows.length > 0 | logged.length > 0) {
                            console.log("Login: Somebody is already using the lockers");
                            res.send("0");
                        } else {
                            pool.query('INSERT INTO logged (card, time) VALUES ($1, $2)', [req.query.id, new Date()], (error, results) => {
                                if (error) {
                                    throw error
                                }
                                console.log("Login: OK");
                                res.send("1");
                            })

                        }

                    })

                })
            }
        })
    }
});

app.get('/connect_locker', (req, res) => {

    if (req.query.locker_id == undefined | req.query.tag_id == undefined) {
        throw new Error('Undefined field found');
    } else {
        pool.query('SELECT * FROM locker WHERE id = $1', [req.query.locker_id], (error, results) => {
            if (error) {
                throw error;
            } else {
                if (results.rows.length > 0) {
                    res.send(results.rows[0].key);
                } else {
                    pool.query('INSERT INTO locker (id, key, open, keyin, enabled) VALUES ($1,$2,false,false,true)'
                        , [req.query.locker_id, req.query.tag_id],
                        (error, results) => {
                            if (error) {
                                throw (error);
                            } else {
                                res.send(req.query.tag_id);
                            }

                        })
                }
            }
        })

    }

});

app.get('/tag_setup', (req, res) => {
    if (req.query.locker_id == undefined | req.query.tag_id == undefined | req.query.tag_in == undefined) {
        throw new Error('Undefined field found');
    } else {
        pool.query('SELECT * FROM locker WHERE id = $1 AND key = $2', [req.query.locker_id, req.query.tag_id], (error, results) => {
            if (error) {
                throw error;
            } else {
                if (results.rows.length == 0) {
                    throw "No matching locker";
                } else {
                    pool.query('UPDATE locker SET keyin = $1 WHERE id = $2 AND key = $3'
                        , [req.query.tag_in, req.query.locker_id, req.query.tag_id],
                        (error, results) => {
                            if (error) {
                                throw (error);
                            } else {
                                res.send(req.query.tag_id);
                            }

                        })
                }
            }
        })
    }
});

app.get('/open', (req, res) => {
    if (req.query.locker_id == 'undefined') {
        throw new Error('BROKEN');
    } else {

        pool.query('SELECT * FROM logged', (error, results) => {
            if (error) {
                throw error
            }
            if (results.rows.length > 0) {
                let logged = results.rows[0];
                pool.query('SELECT * FROM locker WHERE open', (error, results) => {
                    if (error) {
                        throw error
                    }
                    if (results.rows.length > 0) {
                        console.log("Open: a locker is already open");
                        res.send("0");
                    } else {

                        pool.query('UPDATE locker SET open = true WHERE id = $1 AND enabled', [req.query.locker_id], (error, results) => {
                            if (error) {
                                throw (error);
                            } else {
                                let login = logged;
                                pool.query('SELECT * FROM locker WHERE id = $1 AND open AND enabled', [req.query.locker_id], (error, results) => {
                                    if (error) {
                                        trhow(error);
                                    } else if (results.rows.length > 0) {
                                        let locker = results.rows[0];
                                        if (results.rows[0].id == req.query.locker_id) {
                                            pool.query('INSERT INTO access (card, locker, key, keyinbefore, timeopen) VALUES ($1,$2,$3,$4,$5)'
                                                , [login.card, locker.id, locker.key, locker.keyin, new Date()],
                                                (error, results) => {
                                                    if (error) {
                                                        pool.query('UPDATE locker SET open = false WHERE id = $1 AND enabled', [req.query.locker_id], (error, results) => {
                                                            if (error) {
                                                                throw (error);
                                                            } else {
                                                                pool.query('DELETE FROM logged', (error, results) => {
                                                                    res.send("0");
                                                                })
                                                            }
                                                        })
                                                    } else {
                                                        pool.query('DELETE FROM logged', (error, results) => {
                                                            if (error) {
                                                                throw (error);
                                                            } else {
                                                                console.log("Open: Access agreed, delete login");
                                                                res.send("1:" + locker.key);
                                                            }
                                                        })
                                                    }

                                                })
                                        } else {
                                            pool.query('DELETE FROM logged', (error, results) => {
                                                if (error) {
                                                    throw (error);
                                                } else {
                                                    console.log("Open: Another locker is already open");
                                                    res.send("0");
                                                }
                                            })
                                        }
                                    } else {
                                        pool.query('DELETE FROM logged', (error, results) => {
                                            if (error) {
                                                throw (error);
                                            } else {
                                                cconsole.log("Open: locker not opened");
                                                res.send("0");
                                            }
                                        })

                                    }
                                })

                            }
                        })
                    }
                })
            } else {
                console.log("Open: no logged found");
                res.send("0");
            }
        })
    }
});

app.get('/close', (req, res) => {

    if (req.query.locker_id == undefined) {
        throw new Error('BROKEN');
    } else {
        let locker_id = req.query.locker_id;
        let tag_id = req.query.tag_id;
        let tag_in = (req.query.tag_in == "0") ? false : true;

        pool.query('SELECT * FROM locker WHERE id = $1 AND open', [locker_id], (error, results) => {
            if (error) {
                throw error
            } else {
                console.log("Close: " + tag_in + " " + tag_id + " " + results.rows[0].key);
                if (results.rows.length > 0) {
                    pool.query('UPDATE locker SET open = false, keyin=$2 WHERE id = $1', [locker_id, tag_in], (error, results) => {
                        if (error) {
                            throw (error);
                        } else {
                            pool.query('UPDATE access SET keyinafter = $1, timeclose = $2 WHERE locker = $3 AND timeopen = (SELECT MAX(timeopen) FROM access WHERE locker = $3)',
                                [tag_in, new Date(), locker_id],
                                (error, results) => {
                                    if (error) {
                                        throw (error);
                                    } else {
                                        console.log("Close: ok");
                                        res.send("1");
                                    }
                                })
                        }
                    })
                } else {
                    console.log("Close: no open locker found");
                    res.send("0");
                }
            }
        })


    }
});

app.get('/isLogged', (req, res) => {

    pool.query('SELECT * FROM logged', (error, results) => {
        if (error) {
            throw error
        } else {
            if (results.rows.length > 0) {
                res.send("1");
            } else {
                res.send("0");
            }
        }
    })



});









app.listen(port, () => {

    console.log(`App listening on container port ${port}`);
});